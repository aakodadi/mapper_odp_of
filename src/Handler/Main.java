package Handler;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.w3c.dom.DOMException;
import org.xml.sax.SAXException;

import struct.Application;
import struct.Concept;
import struct.Device;
import struct.Lattice;
import struct.LogicalTable;
import struct.PhysicalTable;

public class Main {
    
    public static void main(String[] args) throws DOMException, SAXException,
            IOException, ParserConfigurationException, InterruptedException,
            TransformerException {
		// TODO Auto-generated method stub
        
        if (args.length < 5) {
            System.out.println("Error: Missing Argument(s)!");
            System.exit(1);
        }
        DeviceReader physical_reader = new DeviceReader(0);
        String device_filename = args[0];
        
        Device Device_name = physical_reader
                .ReadPhysicalProfil(device_filename);
        //System.out.println(Device_name);

        ApplicationReader app_reader = new ApplicationReader(0);
        String app_filename = args[1];
        Application App_name = app_reader.ReadPhysicalProfil(app_filename);
        //System.out.println(App_name);
        RCFGeneration rcfgeneration = new RCFGeneration(0, Device_name,
                App_name);
        rcfgeneration.CreateMatrix();
        
        String rcf_filename = args[2];
        String xml_filename = args[3];
        String mapping_path = args[4];
        rcfgeneration.CreateRCFFile(rcf_filename);

		// rcfgeneration.VerifyCrossTable();
        LatticeGeneration lattice_generation = new LatticeGeneration(0);
        lattice_generation.GenerateLattice(rcf_filename, xml_filename);
        
        LatticeReader lattice_reader = new LatticeReader(0);
        Lattice Lattice_name = lattice_reader.ReadLattice(App_name,
                Device_name, xml_filename);
        //System.out.println(Lattice_name);
        ConceptEvaluation cpt_eval = new ConceptEvaluation(0);
        Map<Integer, Concept> list_concept = Lattice_name.getList_concept();
		// for(int i=0;i<list_concept.size();i++){
        // Concept c = list_concept.get(i+1);
        // System.out.println("Concept : "+c.getId());
        // System.out.println("\t The capacity evaluation of concept  is : "+cpt_eval.Capacity_evaluation(c));
        // System.out.println("\t The size evaluation of concept  is : "+cpt_eval.Size_evaluation(c));
        // }
        // System.out.println(App_name.getPipeline().get(0).getMatchfields().get(0).getFields().get(0).getType().getLenght());
		/*
         * Map<Integer, List<Integer>> list = cpt_eval.generatesubset(3);
         * for(int i=1;i<list.size();i++){
         * System.out.println("\n The "+(i)+" combinaison is :"); for(int
         * j=0;j<list.get(i).size();j++){
         * System.out.print(" "+list.get(i).get(j)); } }
         */
        List<Integer> test = new LinkedList<Integer>();
        test.add(0);
        test.add(1);
        test.add(3);

		// System.out.println(Lattice_name);
        // System.out.println(App_name);
        // System.out.println("\nKey size required is : "+cpt_eval.calculateSizeTables(App_name,
        // test));
        // System.out.println("Capacity required is : "+cpt_eval.calculateCapacityTables(App_name,
        // test));
        Thread.sleep(1000);
        Map<Concept, Map<PhysicalTable, ArrayList<LogicalTable>>> list_pot_mapping = cpt_eval
                .PrepareAnnotattedConcept(App_name, Lattice_name);
        
        Map<PhysicalTable, ArrayList<LogicalTable>> mapping = cpt_eval
                .Browse_Select_ConceptLattice(App_name, Device_name,
                        list_pot_mapping);
        
        MappingHandler mapHandler = new MappingHandler();
        boolean ret = mapHandler.MappingGenerator(mapping_path, mapping, App_name,
                Device_name);
        if (!ret) {
            System.err.println("ERROR when generating the mapping!");
        }
    }
    
}
