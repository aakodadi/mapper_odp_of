package Handler;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import struct.Application;
import struct.Attribute;
import struct.Concept;
import struct.Device;
import struct.Lattice;
import struct.LogicalTable;
import struct.Object;
import struct.PhysicalTable;

public class LatticeReader {

	int id;

	public LatticeReader(int id) {
		super();
		this.id = id;
	}
	Lattice  ReadLattice(Application app,Device device, String filename) throws ParserConfigurationException, SAXException, IOException{
		
		// Opening XML File For Processing
		String filepath = filename;
		DocumentBuilderFactory docFactory = DocumentBuilderFactory
				.newInstance();
		DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
		Document doc = docBuilder.parse(filepath);
		//Create id and concept of lattice
		int id_lattice=0;
		Map<Integer, Concept> list_concept = new HashMap<Integer, Concept>();
		
		// Get attribute and object
			Map<Integer, LogicalTable> attribute = app.getPipeline();
			Map<Integer, PhysicalTable> object = device.getTables();
		
		//Get concepts in xml and parse them
		NodeList concepts =  doc.getElementsByTagName("concept");		
		for(int i=0;i<concepts.getLength();i++){
		Element concept = (Element) concepts.item(i);			
		//Initialize The fields of the concept
		Integer id_concept = Integer.parseInt(concept.getAttribute("id"));	
		Map<Integer, LogicalTable> list_logical_tables = new HashMap<Integer,LogicalTable>();
		Map<Integer, PhysicalTable> list_physical_tables = new HashMap<Integer, PhysicalTable>();
		
		//Get list of attribute and object
		Element attributes = (Element)(concept.getElementsByTagName("intent").item(0));
		Element objects = (Element)(concept.getElementsByTagName("extent").item(0));
		String[] s_att_list = attributes.getTextContent().split(",");
		String[] s_obj_list = objects.getTextContent().split(",");
		int att_size = Integer.parseInt(attributes.getAttribute("size"));
		int obj_size = Integer.parseInt(objects.getAttribute("size"));
		
		// The following test is used to verify if the concept has at least one object and one attribute
		if(obj_size==0 || att_size==0) continue;
		//Fill and insert attribute and object into their lists.
		for(int j=0;j<s_att_list.length;j++){
			int att = Integer.parseInt(s_att_list[j]);

			LogicalTable l_table = attribute.get(att-1);
			list_logical_tables.put(j, l_table);
		}
		
		for(int j=0;j<s_obj_list.length;j++){
			int obj = Integer.parseInt(s_obj_list[j]);
			PhysicalTable p_table = object.get(obj-1);
			list_physical_tables.put(j, p_table);
		}
		Concept c = new Concept(id_concept, list_logical_tables, list_physical_tables, 0, 0, 0);
		list_concept.put(id_concept, c);	
		}
		Lattice lattice = new Lattice(id_lattice, list_concept);
		return lattice;
	}	
}
