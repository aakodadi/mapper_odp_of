package Handler;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.paukov.combinatorics.Factory;
import org.paukov.combinatorics.Generator;
import org.paukov.combinatorics.ICombinatoricsVector;
import org.paukov.combinatorics.composition.IntegerCompositionGenerator;

import struct.Application;
import struct.Concept;
import struct.Device;
import struct.Field;
import struct.Header;
import struct.Lattice;
import struct.LogicalTable;
import struct.PhysicalTable;
import struct.SearchType;

public class ConceptEvaluation {

    int id;

    public ConceptEvaluation(int id) {
        super();
        this.id = id;
    }

    /*
     * public Map<Integer, Concept> Get_required_Concept(Map<Integer, Concept>
     * list_c){
     * 
     * Map<Integer, Concept> result = new HashMap<Integer, Concept>(); for(int
     * i=0;i<list_c.size();i++){ Concept concept = list_c.get(i);
     * if(concept.getList_logical_tables().size()==1) } return result; }
     */
    public Map<Integer, List<Integer>> generatesubset(int n) {
        Map<Integer, List<Integer>> result = new HashMap<Integer, List<Integer>>();
        // Create an initial vector/set
        ICombinatoricsVector<Integer> initialSet = Factory.createVector();
        for (int i = 0; i < n; i++) {
            initialSet.addValue(i);// vec[i]=i;
        }

        // Create an instance of the subset generator
        Generator<Integer> gen = Factory.createSubSetGenerator(initialSet);

        // Print the subsets
        int i = 0;
        for (ICombinatoricsVector<Integer> subSet : gen) {
            List<Integer> list = subSet.getVector();
            // if(i==0){i++;}
            result.put(i, list);
            i++;
        }
        return result;
    }

    public int calculateSizeTables(Application app, List<Integer> list) {
        Set<Field> matchfield = new HashSet<Field>();
        int table;
        for (int i = 0; i < list.size(); i++) {
            table = list.get(i);
            for (int j = 0; j < app.getPipeline().get(table).getMatchfields()
                    .size(); j++) {
                for (int e = 0; e < app.getPipeline().get(table)
                        .getMatchfields().get(j).getFields().size(); e++) {
                    Field field = app.getPipeline().get(table).getMatchfields()
                            .get(j).getFields().get(e);
                    Iterator<Field> iter_field = matchfield.iterator();
                    int index = 0;
                    while (iter_field.hasNext()) {
                        if (field.toString().equals(
                                iter_field.next().toString())) {
                            index = 1;
                        }
                    }
                    if (index == 1) {
                        continue;
                    } else {
                        matchfield.add(field);
                    }

                }
            }
        }
        int result = 0;
        // System.out.println("Size is : " + matchfield.size());
        Iterator<Field> iter = matchfield.iterator();
        while (iter.hasNext()) {
            result += iter.next().getSize();
        }
        return result;
    }

    public int calculateCapacityTables(Application app, List<Integer> list) {
        int cap = 0;
        Set<LogicalTable> set_table = new HashSet<>();
        Set<LogicalTable> used_tables = new HashSet<>();
        for (int i = 0; i < list.size(); i++) {
            set_table.add(app.getPipeline().get(list.get(i)));
        }
        Iterator<LogicalTable> iter_tables = set_table.iterator();
        while (iter_tables.hasNext()) {
            LogicalTable table = iter_tables.next();
            // if(!used_tables.contains(table)){
            calculateCapacityTable(app, set_table, used_tables, table, list);

            // }
        }
        // Iterator<LogicalTable> iter_tab = used_tables.iterator();
        iter_tables = set_table.iterator();
        while (iter_tables.hasNext()) {
            LogicalTable table = iter_tables.next();
            if (!used_tables.contains(table)) {
                cap += calculateCapacityTable(app, set_table, used_tables,
                        table, list);
            }
        }

        return cap;

    }

    public int calculateCapacityTable(Application app,
            Set<LogicalTable> set_table, Set<LogicalTable> used_tables,
            LogicalTable table, List<Integer> list) {
        int cap = 0;

        List<Integer> next_table = table.getAction().getNext_table();
        if (next_table.size() == 1 && next_table.get(0) == -1) { // no go to
            // table
            return table.getRequired_capacity();
        } else {
            int e = 0;
            for (int i = 0; i < next_table.size(); i++) {
                int inext = next_table.get(i);
                if (list.contains(inext)) {
                    e = 1;
                    break;
                }
            }
            if (e == 0) {
                return table.getRequired_capacity();
            } else {
                for (int j = 0; j < next_table.size(); j++) {
                    int next = next_table.get(j);
                    if (list.contains(next)) {
                        LogicalTable tab = app.getPipeline().get(next);
                        used_tables.add(tab);
                        cap += Math.max(
                                table.getRequired_capacity(),
                                calculateCapacityTable(app, set_table,
                                        used_tables, tab, list));
                    }
                }
            }
        }

        return cap;
    }

    public double Size_evaluation(Concept c) {
        Map<Integer, LogicalTable> list_logical_tables = c
                .getList_logical_tables();
        Map<Integer, PhysicalTable> list_physical_tables = c
                .getList_physical_tables();

        double size_eval = 0;

        for (int j = 0; j < list_physical_tables.size(); j++) {
            size_eval += list_physical_tables.get(j).getSize();
        }
        for (int i = 0; i < list_logical_tables.size(); i++) {
            size_eval -= list_logical_tables.get(i).getRequired_size();
        }

        return size_eval;
    }

    public double Capacity_evaluation(Concept c) {
        Map<Integer, LogicalTable> list_logical_tables = c
                .getList_logical_tables();
        Map<Integer, PhysicalTable> list_physical_tables = c
                .getList_physical_tables();

        double capacity_eval = 0;

        for (int j = 0; j < list_physical_tables.size(); j++) {
            capacity_eval += list_physical_tables.get(j).getNbr_entries();
        }
        for (int i = 0; i < list_logical_tables.size(); i++) {
            capacity_eval -= list_logical_tables.get(i).getRequired_capacity();
        }

        return capacity_eval;
    }

    public Map<Concept, Map<PhysicalTable, ArrayList<LogicalTable>>> PrepareAnnotattedConcept(
            Application app, Lattice lattice) {

        Map<Concept, Map<PhysicalTable, ArrayList<LogicalTable>>> list_pot_mapping = new HashMap<Concept, Map<PhysicalTable, ArrayList<LogicalTable>>>();

		// System.out.println("list of concept is "+lattice.getList_concept().size());
        // System.out.println("The size of concept is "+lattice.getList_concept().get(5).getList_physical_tables().size());
        for (int i = 1; i <= lattice.getList_concept().size(); i++) {

            Map<PhysicalTable, ArrayList<LogicalTable>> physic_logic = new HashMap<PhysicalTable, ArrayList<LogicalTable>>();

            Concept c = lattice.getList_concept().get(i);
			// System.out.println("               Verify Concept : "+c.getId());
            // System.out.println("The number of physical tables : "+lattice.getList_concept().get(i).getList_physical_tables().size());
            // System.out.println("   The size of logical tables : "+lattice.getList_concept().get(i).getList_logical_tables().size());
            Set<LogicalTable> log_set = new HashSet<>();

            for (int j = 0; j < c.getList_logical_tables().size(); j++) {
                log_set.add(c.getList_logical_tables().get(j));
				// System.out.println("Adding the logical table "+
                // c.getList_logical_tables().get(j).getId());
            }

            for (int j = 0; j < c.getList_physical_tables().size(); j++) {
                PhysicalTable physicaltable = c.getList_physical_tables()
                        .get(j);
                // System.out.println("\nHandling the physical table : "+physicaltable.getId());

                Map<Integer, List<Integer>> list_subset = generatesubset(log_set
                        .size());
                // System.out.println("The number of logical table to handle is "+log_set.size());

                ArrayList<LogicalTable> logical_list = new ArrayList<LogicalTable>(
                        log_set);
                ArrayList<LogicalTable> logical_mapping = new ArrayList<LogicalTable>();
                for (int l = 0; l < logical_list.size(); l++) {
                    // System.out.println("Logical table "+l+" is "+logical_list.get(l).getId());
                }

                int max_size_logical_table = 0;
                int best_fit_factor_mapping = Integer.MAX_VALUE;

                for (int k = 1; k < list_subset.size(); k++) {
                    int current_max_size_log = 0;
                    List<Integer> list = list_subset.get(k);
                    List<Integer> list_tables = new ArrayList<Integer>();
					// System.out.println("Work on list : "+k+
                    // " list size : "+list.size());

                    for (int e = 0; e < list.size(); e++) {
						// System.out.println("Add logical table "+logical_list.get(list.get(e)).getId()
                        // );

                        list_tables.add(logical_list.get(e).getId());
                        if (logical_list.get(e).getRequired_size() > current_max_size_log) {
                            current_max_size_log = logical_list.get(e)
                                    .getRequired_size();
                            // System.out.println(" : "+current_max_size_log);
                        }
                    }

                    int size = calculateSizeTables(app, list_tables);
                    int capacity = calculateCapacityTables(app, list_tables);
                    int current_fit = physicaltable.getSize() - size;
                    // System.out.println("The curremt fit is :"+ current_fit);
                    if (size < physicaltable.getSize()
                            && capacity < physicaltable.getNbr_entries()
                            && (current_max_size_log >= max_size_logical_table || current_fit < best_fit_factor_mapping)) {
                        // System.out.print("\t\t A new better mapping is found : ");
                        logical_mapping = new ArrayList<LogicalTable>();
                        for (int l = 0; l < list.size(); l++) {
                            logical_mapping.add(c.getList_logical_tables().get(
                                    list.get(l)));
                            // System.out.print(" table "+c.getList_logical_tables().get(list.get(l)).getId()+" |");
                        }
                        max_size_logical_table = current_max_size_log;
                        best_fit_factor_mapping = current_fit;
                        // System.out.println("\n\t\t the new max size is "+max_size_logical_table+" best fit is "+current_fit);
                    }
                }

                physic_logic.put(physicaltable, logical_mapping);
				// System.out.println("Set size before is "+log_set.size());
                // supprimer reddondance
                // if(!log_set.isEmpty()){
                // log_set.removeAll(logical_mapping);}
                // System.out.println("Set size after is "+log_set.size());
            }
            list_pot_mapping.put(c, physic_logic);
        }

		// Map<Concept, Map<PhysicalTable, ArrayList<LogicalTable>>>
		/*
         * System.out.println("\n\t\t\tThe mapping is :"); Set<Concept> c_set =
         * list_pot_mapping.keySet(); Iterator<Concept> c_iter =
         * c_set.iterator(); while(c_iter.hasNext()){ Concept c = c_iter.next();
         * System.out.println("\nConcept id is "+c.getId()); Map<PhysicalTable,
         * ArrayList<LogicalTable>> map = list_pot_mapping.get(c);
         * Set<PhysicalTable> p_set = map.keySet(); Iterator<PhysicalTable>
         * p_iter = p_set.iterator(); while(p_iter.hasNext()){ PhysicalTable p_t
         * = p_iter.next(); System.out.println("\nPhysical table is : "+
         * (p_t.getId()) + " - " + p_t.getSearch()); ArrayList<LogicalTable>
         * l_list = map.get(p_t); if(!l_list.isEmpty()){ for(int
         * e=0;e<l_list.size();e++){ System.out.print(l_list.get(e).getId() +
         * "(" + l_list.get(e).getRequired_search() +")   |   "); } } } }
         */
        return list_pot_mapping;

    }

    public Map<PhysicalTable, ArrayList<LogicalTable>> Browse_Select_ConceptLattice(
            Application app,
            Device dev,
            Map<Concept, Map<PhysicalTable, ArrayList<LogicalTable>>> AnnotatedConcept_list) {

        Map<PhysicalTable, ArrayList<LogicalTable>> mapping = new HashMap<PhysicalTable, ArrayList<LogicalTable>>();

        ArrayList<LogicalTable> ltable_list = new ArrayList<LogicalTable>();
        // On recupere la liste complete des tables logiques
        Map<Integer, LogicalTable> lt_list = app.getPipeline();
        for (Map.Entry<Integer, LogicalTable> t : lt_list.entrySet()) {
            ltable_list.add(t.getValue());
            // System.out.println("table logique " + t.getValue().getId());
        }

        ArrayList<PhysicalTable> ptable_list = new ArrayList<PhysicalTable>();
        // On recupere la liste complete des tables physiques
        Map<Integer, PhysicalTable> pt_list = dev.getTables();
        for (Map.Entry<Integer, PhysicalTable> t : pt_list.entrySet()) {
            ptable_list.add(t.getValue());
            // System.out.println("table physique " + t.getValue().getId());
        }

        ArrayList<LogicalTable> mapped_ltable_list = new ArrayList<LogicalTable>();

		// Parcourir la liste des tables physiques
        // pour la selection d'un mapping
        for (PhysicalTable pt : ptable_list) {

            int id = 0;
            // Initialiser une liste de mapping pour la table physique
            Map<Integer, ArrayList<LogicalTable>> map_pt = new HashMap<Integer, ArrayList<LogicalTable>>();

			// Pour chaque concept, on récupere les mappings associes a la table
            // physique
            for (Entry<Concept, Map<PhysicalTable, ArrayList<LogicalTable>>> m : AnnotatedConcept_list
                    .entrySet()) {

                Map<PhysicalTable, ArrayList<LogicalTable>> map = m.getValue();

                if (!map.containsKey(pt)) {
                    continue;
                }

                // On recupere le set de tables logiques
                ArrayList<LogicalTable> set = map.get(pt);

                // Verifier si les tables du set existe deja dans un mapping
                int red = 0;
                for (LogicalTable lt : set) {
                    if (mapped_ltable_list.contains(lt)) {
                        red++;
                    }
                }

                // si le set existe deja, on passe au concept suivant
                if (red == set.size()) {
                    continue;
                }

                // Ajouter le set au mapping de la table physique
                map_pt.put(id, set);
                id++;

            }

			// Initialiser une liste de mapping pour la table physique (premiere
            // verification)
            Map<Integer, ArrayList<LogicalTable>> map_pt_st = new HashMap<Integer, ArrayList<LogicalTable>>();

            // Verifier LPM vs Exact
            boolean change = false;
            int cnt = 0;
            for (Entry<Integer, ArrayList<LogicalTable>> map : map_pt
                    .entrySet()) {
                boolean keep = false;
                // On récupère le mapping
                ArrayList<LogicalTable> set = map.getValue();
				// Pour chaque mapping on verifie si on lui associe a un lpm ou
                // un exact
                for (LogicalTable lt : set) {
                    // On recupère les matchfields
                    Map<Integer, Header> match = lt.getMatchfields();
                    // On verifie si c'est lpm ou exact (lpm:0 exact:1)
                    if (checkSearch(match)) {
                        if (pt.getSearch().equals(SearchType.EXACT)) {
                            keep = true;
                            change = true;
                        }
                    } else {
                        if (pt.getSearch().equals(SearchType.LPM)) {
                            keep = true;
                            change = true;
                        } else if (pt.getSearch().equals(SearchType.TERNARY)) {
                            keep = true;
                            change = true;
                        }
                    }
                }

                // On met a jour le mapping qu'on va garder
                if (keep) {
                    map_pt_st.put(cnt, set);
                    cnt++;
                }

            }

			// S'il n'existe pas de type de recherche associe a la table
            // physique on garde toutes les combinaisons
            if (!change) {
                map_pt_st = map_pt;
            }

			// Initialiser une liste de mapping pour la table physique (deuxieme
            // verification)
            Map<Integer, ArrayList<LogicalTable>> map_pt_tt = new HashMap<Integer, ArrayList<LogicalTable>>();

            // Verifier la taille pour les differents mappings
            int max = -1;
            boolean keep = false;
            int map_id = 0;
            // On recupere l'ensemble des mappings (restants)
            for (Entry<Integer, ArrayList<LogicalTable>> map : map_pt_st
                    .entrySet()) {
				// On recupere les id des tables logiques pour calculer la
                // taille totale
                List<Integer> lt_id_list = new LinkedList<Integer>();
                for (LogicalTable lt : map.getValue()) {
                    lt_id_list.add(lt.getId());
                }
                int size = calculateCapacityTables(app, lt_id_list);
                if (size > max) {
                    max = size;
                    map_id = map.getKey();
                } else if (size == max) {
                    // Si deux mappings ont la meme taille, on les garde
                    map_pt_tt.put(map_id, map.getValue());
                }
            }

            // On garde la plus grande combinaison
            if (!map_pt_tt.containsKey(map_id)) {
                map_pt_tt.put(map_id, map_pt_st.get(map_id));
            }

            // Sélection du mapping optimale
            if (map_pt_tt.size() > 1) {
                // On recupere le mapping
                for (Entry<Integer, ArrayList<LogicalTable>> m : map_pt_tt
                        .entrySet()) {
                    int cnt_t = 0;
					// On rajoute la liste des tables logiques a la liste des
                    // tables mappees
                    for (LogicalTable lt : m.getValue()) {
                        if (mapped_ltable_list.contains(lt)) {
                            cnt_t++;
                        }
                    }
                    if (cnt_t < m.getValue().size()) { // Peut causer des
                        // redondances
                        // On recupere le mapping
                        mapping.put(pt, m.getValue());
						// On rajoute la liste des tables logiques a la liste
                        // des tables mappees
                        for (LogicalTable lt : m.getValue()) {
                            if (!mapped_ltable_list.contains(lt)) {
                                mapped_ltable_list.add(lt);
                            }
                        }
                    }
                }
            } else {
				// On recupere le mapping
                //System.out.println("The mapping is \n"+map_pt_tt);
                for (Entry<Integer, ArrayList<LogicalTable>> m : map_pt_tt
                        .entrySet()) {
                    if (m.getValue() == null) {
                        continue;
                    }
                    mapping.put(pt, m.getValue());
					// On rajoute la liste des tables logiques a la liste des
                    // tables mappees
                    for (LogicalTable lt : m.getValue()) {
                        if (!mapped_ltable_list.contains(lt)) {
                            mapped_ltable_list.add(lt);
                        }
                    }
                }
            }

        }

        // Gérer les redondances
        for (Entry<PhysicalTable, ArrayList<LogicalTable>> map : mapping
                .entrySet()) {
            for (LogicalTable lt : map.getValue()) {
				// Verifier si la table logique ne se trouve pas dans un autre
                // mapping
                for (Entry<PhysicalTable, ArrayList<LogicalTable>> m : mapping
                        .entrySet()) {
                    if (!m.getKey().equals(map.getKey())) {
                        if (m.getValue().contains(lt)) {
                            if (m.getValue().size() > map.getValue().size()) {
                                ArrayList<LogicalTable> list = new ArrayList<LogicalTable>();
                                for (LogicalTable l : m.getValue()) {
                                    if (!l.equals(lt)) {
                                        list.add(l);
                                    }
                                    m.setValue(list);
                                }
                            } else {
                                if (map.getValue().size() - 1 != 0) {
                                    ArrayList<LogicalTable> list = new ArrayList<LogicalTable>();
                                    for (LogicalTable l : map.getValue()) {
                                        if (!l.equals(lt)) {
                                            list.add(l);
                                        }
                                        map.setValue(list);
                                    }
                                }
                            }
                        }
                    }
                }

            }
        }

        // Verifier la couverture des tables logiques
        ArrayList<LogicalTable> list = new ArrayList<LogicalTable>();
        for (Entry<PhysicalTable, ArrayList<LogicalTable>> m : mapping
                .entrySet()) {
            for (LogicalTable lt : m.getValue()) {
                if (!list.contains(lt)) {
                    list.add(lt);
                }
            }
        }

        if (list.size() != ltable_list.size()) {
            System.err.println("Les tables logiques ne sont pas couvertes\n");
            // TODO gerer la couverture des tables
            for (Map.Entry<Integer, LogicalTable> t : lt_list.entrySet()) {
                if (!list.contains(t.getValue())) {
                    System.out.println("table logique qui manque = "
                            + t.getValue().getId());
                    System.exit(2);
                }
            }

        }

		// Affichage du mapping
		/*
         * for (Entry<PhysicalTable, ArrayList<LogicalTable>> m : mapping
         * .entrySet()) { System.out.println(m.getKey().getId() + " :"); for
         * (LogicalTable lt : m.getValue()) { System.out.println("\t" +
         * lt.getId()); } }
         */

        /*
         * for (Entry<Integer, ArrayList<LogicalTable>> a :
         * map_pt_st.entrySet()) { System.out.println( "\t\tmap id = " +
         * a.getKey()); for (LogicalTable lt : a.getValue()) {
         * System.out.println("\t\t\tlist_lt = " + lt.getId()); } }
         * 
         * System.out.println("-------" );
         */
        return mapping;

    }

    private boolean checkSearch(Map<Integer, Header> match) {

        int cnt_lpm = 0;
        int cnt_exact = 0;

        for (Entry<Integer, Header> hdr : match.entrySet()) {
            Map<Integer, Field> fields = hdr.getValue().getFields();
            for (Entry<Integer, Field> fd : fields.entrySet()) {

                if (fd.getValue().getType().getSearchType()
                        .equals(SearchType.EXACT)) {
                    cnt_exact++;
                } else if (fd.getValue().getType().getSearchType()
                        .equals(SearchType.LPM)) {
                    cnt_lpm++;
                }

            }
        }

        if (cnt_lpm < 0) {
            return true;
        } else {
            return false;
        }

    }

}
