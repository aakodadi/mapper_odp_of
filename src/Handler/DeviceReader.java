package Handler;


import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import struct.Device;
import struct.DeviceType;
import struct.Memory;
import struct.PhysicalTable;
import struct.SearchType;
import struct.TableType;

public class DeviceReader {
	
	public DeviceReader(int id) {
		super();
		this.id = 0;
	}
	int id;
	public Device ReadPhysicalProfil(String  filename) throws SAXException, IOException, ParserConfigurationException {
		
		// Opening xml File for processing   
		String filepath = filename;
		DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
		Document doc = docBuilder.parse(filepath);
		
		// Get device id and label
		Element device = (Element) doc.getFirstChild();
		int id_device = Integer.parseInt(device.getAttribute("Id"));
		String label_device = device.getAttribute("Label");
		
		
		// Get Device Global Capability
		NodeList device_capability =  doc.getElementsByTagName("Capability");
		int capacity = Integer.parseInt((((Element) device_capability.item(0)).getAttribute("Capacity")));
		int clock = Integer.parseInt((((Element) device_capability.item(0)).getAttribute("Clock")));
		int interface_f = Integer.parseInt((((Element) device_capability.item(0)).getAttribute("Interface")));
		
		
		Map<Integer, PhysicalTable> device_tables = new HashMap<Integer, PhysicalTable>(); 
		
		// Create The device				
		Device devicename = new Device(id_device,label_device,"",capacity,clock,interface_f,device_tables);
				
		
		// Get tables and their characteristics
		
		PhysicalTable physicaltable ;
		NodeList TablesList = doc.getElementsByTagName("Table");
		for(int i=0;i<TablesList.getLength();i++){
			Element table = (Element)TablesList.item(i);
			int id_table = Integer.parseInt(table.getAttribute("Id"));
			int nbrentry = Integer.parseInt(table.getAttribute("NbEntry"));
			int key_size  = Integer.parseInt(table.getAttribute("Key"));
			String type = table.getAttribute("Type");
			String search = table.getAttribute("Search");
			NodeList capability = table.getElementsByTagName("Capability");			
			int capacity_table = Integer.parseInt(((Element) (capability.item(0))).getAttribute("Capacity"));
			int speed_table = Integer.parseInt(((Element) (capability.item(0))).getAttribute("Speed"));
			
			//Table table specification
			TableType tabletype = TableType.valueOf(type);
			SearchType searchtype = SearchType.valueOf(search);
			Memory memory = Memory.valueOf(table.getAttribute("Memory"));

			//Create Physical Table
			physicaltable = new PhysicalTable(id_table, searchtype, tabletype, key_size, nbrentry,capacity_table ,speed_table, memory);
			//Add the table to the device
			devicename.getTables().put(i, physicaltable);
		}
		return devicename;       	
		
	}
}
