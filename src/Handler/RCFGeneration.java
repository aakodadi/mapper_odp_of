package Handler;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;

import struct.Application;
import struct.Device;
import struct.SearchType;

public class RCFGeneration {

    int id;
    Device device;
    Application application;
    int[][] matrix;

    public RCFGeneration(int id, Device device, Application application) {
        super();
        this.id = id;
        this.device = device;
        this.application = application;
        matrix = new int[device.getTables().size()][application.getPipeline()
                .size()];
    }

    int[][] CreateMatrix() {

        for (int i = 0; i < device.getTables().size(); i++) {
            for (int j = 0; j < application.getPipeline().size(); j++) {
                // Verify Capacity
                if (application.getPipeline().get(j).getRequired_capacity() < this.device
                        .getTables().get(i).getNbr_entries()) {
                    // Verify Key Size
                    if (application.getPipeline().get(j).getRequired_size() < this.device
                            .getTables().get(i).getSize()) {
                        // Verify Type of search
                        if (application.getPipeline().get(j).getRequired_search().getWeight() <= this.device.getTables().get(i).getSearch().getWeight()) {
                            matrix[i][j] = 1;
                        }
                    }
                }
            }
        }

        return matrix;
    }

    void CreateRCFFile(String Filename) {
        int[][] matrice = this.CreateMatrix();
        PrintWriter writer = null;
        try {
            writer = new PrintWriter(Filename, "UTF-8");
        } catch (FileNotFoundException | UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        writer.println("[Relational Context]" + "\n" + "Association Table"
                + "\n" + "[Binary Relation]" + "\n" + "Cross Table");

        // write physical tables
        for (int i = 0; i < matrice.length; i++) {
            writer.print("PhysicalTable" + i + " |");
        }
        writer.println();
        // write logical tables
        for (int i = 0; i < matrice[0].length; i++) {
            writer.print("LogicalTable" + i + " |");
        }
        writer.println();
        for (int i = 0; i < matrice.length; i++) {
            for (int j = 0; j < matrice[0].length; j++) {
                writer.print(matrice[i][j] + " ");
            }
            writer.println();
        }

        writer.print("[END Relational Context]");
        writer.close();

    }

    void VerifyCrossTable() {
        for (int j = 0; j < this.matrix[0].length; j++) {
            int e = 0;
            int temp = 0;
            for (int i = 0; i < this.matrix.length; i++) {
                e += this.matrix[i][j];
                if (this.matrix[i][j] != 0) {
                    temp = i;
                }
            }
            if (e == 0) {
                System.out.println("logical table "
                        + j
                        + " can not be mapped"
                        + "\n"
                        + "The required size is : "
                        + application.getPipeline().get(j).getRequired_size()
                        + "\n"
                        + "The required capacity is : "
                        + application.getPipeline().get(j)
                        .getRequired_capacity());
            }
            if (e == 1) {
                System.out.println("Physical table " + (temp) + " should be mapped against logical table " + j);
            }
        }

        for (int i = 0; i < this.matrix.length; i++) {
            int e = 0;
            int temp = 0;
            for (int j = 0; j < this.matrix[0].length; j++) {
                e += this.matrix[i][j];
                if (this.matrix[i][j] != 0) {
                    temp = j;
                }
            }
            if (e == 0) {
                System.out.println("Physical table " + i
                        + " can not support any table" + "\n" + "The  is : "
                        + device.getTables().get(i).getSize() + "\n"
                        + "The capacity is : "
                        + this.device.getTables().get(i).getNbr_entries());
            }
            if (e == 1) {
                System.out.println("Physical table " + (i) + " should be mapped against logical table " + temp);
            }
        }

    }

}
