package Handler;

import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.swing.text.html.HTML.Tag;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import struct.Action;
import struct.Application;
import struct.ApplyActions;
import struct.Field;
import struct.FieldType;
import struct.Header;
import struct.LogicalTable;
import struct.MetaType;
import struct.SearchType;
import struct.TagType;

public class ApplicationReader {

    public ApplicationReader(int id_reader) {
        super();
        this.id_reader = 0;
    }

    int id_reader;

    public Application ReadPhysicalProfil(String filename)
            throws ParserConfigurationException, SAXException, IOException {
        // Opening xml File for processing
        String filepath = filename;
        DocumentBuilderFactory docFactory = DocumentBuilderFactory
                .newInstance();
        DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
        Document doc = docBuilder.parse(filepath);

        // Get Application name
        Element description = (Element) doc.getFirstChild();
        String name = description.getAttribute("Name");

        // Get application id and SLA
        NodeList SLA_temp = doc.getElementsByTagName("SLA");
        Element SLA = (Element) SLA_temp.item(0);

        NodeList Throughput = SLA.getElementsByTagName("Throughput");
        NodeList Delay = SLA.getElementsByTagName("Delay");

        int Throughput_min = Integer.parseInt(((Element) Throughput.item(0))
                .getAttribute("min"));
        int Throughput_max = Integer.parseInt(((Element) Throughput.item(0))
                .getAttribute("max"));
        int Delay_min = Integer.parseInt(((Element) Delay.item(0))
                .getAttribute("min"));
        int Delay_max = Integer.parseInt(((Element) Delay.item(0))
                .getAttribute("max"));

        Map<Integer, LogicalTable> tables_list = new HashMap<Integer, LogicalTable>();
        Application app = new Application(0, name, Throughput_min,
                Throughput_max, Delay_min, Delay_max, tables_list);

        NodeList pipeline = doc.getElementsByTagName("Pipeline");
        NodeList list_table = ((Element) pipeline.item(0))
                .getElementsByTagName("Table");
        for (int i = 0; i < list_table.getLength(); i++) {

            // Analyzing each Table
            Element table = (Element) list_table.item(i);
            int id_table = Integer.parseInt(table.getAttribute("Id"));
            int nb_entry_table = Integer
                    .parseInt(table.getAttribute("NbEntry"));

			// System.out.println("nb entry is "+nb_entry_table);
            String search = table.getAttribute("Search");
            SearchType searchtype_table = SearchType.valueOf(search);
            int size_table = 0;

            Map<Integer, Header> matchfields = new HashMap<Integer, Header>();
            LogicalTable logicaltable = new LogicalTable(id_table,
                    searchtype_table, size_table, nb_entry_table, matchfields);

            Element Match = (Element) table.getElementsByTagName("Match").item(
                    0);
            NodeList headers = Match.getElementsByTagName("Header");
            for (int j = 0; j < headers.getLength(); j++) {

                // Analyzing each Header
                Element header = (Element) headers.item(j);
                int id_header = Integer.parseInt(header.getAttribute("Id"));
                String Ethertype = header.getAttribute("Ethertype");
                // System.out.println("header type is "+Ethertype);

                Map<Integer, Field> fields_list = new HashMap<Integer, Field>();

                Header header_ = new Header(id_header, Ethertype, fields_list);

                NodeList field_list = header.getElementsByTagName("Field");

                for (int e = 0; e < field_list.getLength(); e++) {
                    // Analyzing each Field
                    Element e_field = (Element) field_list.item(e);
                    int id_field = Integer.parseInt(e_field.getAttribute("Id"));
                    int size_field = Integer.parseInt(e_field
                            .getAttribute("Size"));
                    size_table += size_field;
                    FieldType type_field = FieldType.valueOf(e_field
                            .getAttribute("Name"));
                    Field field = new Field(id_field, size_field, type_field);
                    fields_list.put(e, field);
                }
                matchfields.put(id_header, header_);
            }
            List<Integer> next_table = new LinkedList<Integer>();
            Element e_action = (Element) table.getElementsByTagName("Action")
                    .item(0);
            NodeList go_tables = e_action.getElementsByTagName("GOTO");
            for (int t = 0; t < go_tables.getLength(); t++) {
                Element g_table = (Element) go_tables.item(t);
                next_table.add(t,
                        Integer.parseInt(g_table.getAttribute("Table")));
            }
            Boolean clear = false;
            NodeList clear_action = e_action.getElementsByTagName("CLEAR_ACTIONS");
            if (clear_action.getLength() != 0) {
                clear = true;
            }
            /**
             * Here is code that handles actions
             */
            NodeList apply_action = e_action.getElementsByTagName("APPLY_ACTIONS");
            Set<String> set_ports = new HashSet<String>();
            Set<FieldType> set_set_field = new HashSet<FieldType>();
            Set<MetaType> set_set_meta = new HashSet<MetaType>();
            Set<TagType> set_pop_tag = new HashSet<TagType>();
            Set<TagType> set_push_tag = new HashSet<TagType>();
            if (apply_action.getLength() != 0) {
                Element actions = (Element) apply_action.item(0);
                NodeList output = actions.getElementsByTagName("OUTPUT");
                NodeList set_field = actions.getElementsByTagName("SET_FIELD");
                NodeList set_meta = actions.getElementsByTagName("SET_META");
                NodeList pop_tag = actions.getElementsByTagName("POP_TAG");
                NodeList push_tag = actions.getElementsByTagName("PUSH_TAG");
                if (output.getLength() != 0) {

                    for (int k = 0; k < output.getLength(); k++) {
                        Element out = (Element) output.item(k);

                        String e = out.getAttribute("Port");

                        set_ports.add(e);
                    }

                }
                if (set_field.getLength() != 0) {
                    for (int k = 0; k < set_field.getLength(); k++) {
                        Element field = (Element) set_field.item(k);
                        set_set_field.add(FieldType.valueOf(field.getAttribute("Field")));
                    }
                }
                if (set_meta.getLength() != 0) {
                    for (int k = 0; k < set_meta.getLength(); k++) {
                        Element meta = (Element) set_meta.item(k);
                        set_set_meta.add(MetaType.valueOf(meta.getAttribute("Meta")));
                    }
                }
                if (pop_tag.getLength() != 0) {
                    for (int k = 0; k < pop_tag.getLength(); k++) {
                        Element pop_t = (Element) pop_tag.item(k);
                        set_pop_tag.add(TagType.valueOf(pop_t.getAttribute("Tag")));
                    }
                }
                if (push_tag.getLength() != 0) {
                    for (int k = 0; k < push_tag.getLength(); k++) {
                        Element push_t = (Element) push_tag.item(k);
                        set_push_tag.add(TagType.valueOf(push_t.getAttribute("Tag")));
                    }
                }

            }

            ApplyActions list_action = new ApplyActions(set_ports, set_set_field, set_pop_tag, set_push_tag, set_set_meta);
            //System.out.println(list_action);

            Action action = new Action(next_table, clear, list_action);
            logicaltable.setRequired_size(size_table);
            logicaltable.setAction(action);
            tables_list.put(id_table, logicaltable);
        }

        return app;

    }
}
