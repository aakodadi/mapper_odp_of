package Handler;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import struct.Action;
import struct.Application;
import struct.ApplyActions;
import struct.Device;
import struct.Field;
import struct.FieldType;
import struct.Header;
import struct.LogicalTable;
import struct.MetaType;
import struct.PhysicalTable;
import struct.TagType;

public class MappingHandler {

	public Map<PhysicalTable, ArrayList<LogicalTable>> mapping;
	
	public MappingHandler(){
		mapping = new HashMap<PhysicalTable, ArrayList<LogicalTable>>();
	}
	
	public boolean MappingGenerator(String mappingPath, Map<PhysicalTable, ArrayList<LogicalTable>> map, Application app, Device dev) throws ParserConfigurationException, TransformerException{
		
		boolean ret = true;
		
		mapping = map;
		
		// Affichage du mapping
		/*for (Entry<PhysicalTable, ArrayList<LogicalTable>> m : mapping.entrySet()) {
			System.out.println(m.getKey().getId() + " :");
			for (LogicalTable lt : m.getValue()) {
				System.out.println("\t" + lt.getId());
			}
		}*/
		Map<Integer,Integer> reversemapping = new HashMap<Integer,Integer>();
                for (Entry<PhysicalTable, ArrayList<LogicalTable>> m : mapping.entrySet()){
				PhysicalTable pt = m.getKey();
                                for(int i=0;i<m.getValue().size();i++){
                                    reversemapping.put(m.getValue().get(i).getId(), pt.getId());
                                }
					
				}
                
                
                
		/* Création du fichier XML "mapping.xml" */
		
		// Recuperation d'une instance de la classe "DocumentBuilderFactory"
		final DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		// Creation d'un parseur
		final DocumentBuilder builder = factory.newDocumentBuilder();
		// Creation du document
		final Document document= builder.newDocument();
		
		// Creation de l'element racine "Datapath"
		final Element racine = document.createElement("Datapath");
		document.appendChild(racine);
		
		racine.setAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
		racine.setAttribute("xsi:noNamespaceSchemaLocation", "file:Mapping.xsd");
		racine.setAttribute("Application", app.name);
		racine.setAttribute("Device", dev.label);
		
		// Creation du pipeline 
		final Element pipeline = document.createElement("Pipeline");
		racine.appendChild(pipeline);
		
		// Creation des tables
		// Initialisation de l'id des tables logiques
		int id = 0;
                int mapping_id = 0;
		boolean next = true;
		ArrayList<PhysicalTable> pt_list = new ArrayList<PhysicalTable>();
		while ( next ){
			
			// Récuperer la table physique correspondante a l'id de la table logique
			PhysicalTable pt = null;
			boolean trouve = false;
			for (Entry<PhysicalTable, ArrayList<LogicalTable>> m : mapping.entrySet()){
				for (LogicalTable lt : m.getValue()){
					if ( lt.getId() == id ){
						pt = m.getKey();
						trouve = true;
						continue;
					}
				}
				if (trouve){
					continue;
				}
			}
			if ( (!trouve ) && (id == 0) ){
				System.err.println("ERROR no logical table id equal to zero");
			} else if ((!trouve ) && (id != 0) ){
				next = false;
			} else {
			
				if (pt_list.contains(pt)) {
					id ++;
					continue;
				}
				
				final Element table = document.createElement("Table");
				pipeline.appendChild(table);
				table.setAttribute("Id", Integer.toString(mapping_id));
				table.setAttribute("p-Id", Integer.toString(pt.getId()));
				mapping_id++;
				table.setAttribute("Type", pt.type.name());
				table.setAttribute("Search", pt.getSearch().name());
				table.setAttribute("Memory", pt.memory.name());
				table.setAttribute("KeySize", pt.getSize() + "");
				table.setAttribute("NBEntry", Integer.toString(pt.getNbr_entries()));
				
                                
                                
                                
				// Configurer les matchfields
				final Element match = document.createElement("Match");
				table.appendChild(match);
				int id_header = 0;
				int id_meta = 0;
				ArrayList<FieldType> f_list = new ArrayList<FieldType>(); // list des champs
				ArrayList<FieldType> m_list = new ArrayList<FieldType>(); // list des metas
				for (LogicalTable lt : mapping.get(pt)){
				System.out.println("Logical table " + lt.getId() + " mapped to physical table " + pt.getId());
					for (Entry<Integer, Header> m: lt.getMatchfields().entrySet()){
						
						for(Entry<Integer,Field> f : m.getValue().getFields().entrySet()){
							if ( f.getValue().getType().name().equals("TUNNEL_ID") || f.getValue().getType().name().equals("VRF") || f.getValue().getType().name().equals("INGRESS_PORT") || f.getValue().getType().name().equals("EGRESS_PORT") ){
								if( !m_list.contains(f.getValue().getType()) ){	
									final Element meta = document
											.createElement("Meta");
									match.appendChild(meta);
								
									meta.setAttribute("Id", Integer.toString(id_meta)); 
									meta.setAttribute("Name", f.getValue().getType().name());
									meta.setAttribute("Size", Integer.toString(f.getValue().getSize()) );
								
									id_meta++;
									m_list.add(f.getValue().getType());
								}
							}
						}
						
						final Element header = document.createElement("Header");
						match.appendChild(header);
						
						header.setAttribute("Id", Integer.toString(id_header));
						header.setAttribute("Name", m.getValue().getEtherType());
						
						int id_field = 0;
						
						final Element fields = document.createElement("Fields");
						header.appendChild(fields);
						
						for(Entry<Integer,Field> f : m.getValue().getFields().entrySet()){
							if ( !f.getValue().getType().name().equals("TUNNEL_ID") && !f.getValue().getType().name().equals("VRF") && !f.getValue().getType().name().equals("INGRESS_PORT") && !f.getValue().getType().name().equals("EGRESS_PORT") ){
								
								if( !f_list.contains(f.getValue().getType()) ){	
									final Element field = document.createElement("Field");
									fields.appendChild(field);
								
									field.setAttribute("id", Integer.toString(id_field)); 
									field.setAttribute("Name", f.getValue().getType().name());
									field.setAttribute("Size", Integer.toString(f.getValue().getSize()) );
								
									id_field++;
									f_list.add(f.getValue().getType());
								}
							}
						}
						
						id_header++;
									
					}
				}
				
				id ++;
				pt_list.add(pt);
				
				// Configurer les actions
				/*final Element action = document.createElement("Action");
				table.appendChild(action);
				
				if (id < mapping.size()){
					final Element gototable = document.createElement("GOTO");
					action.appendChild(gototable);
					
					gototable.setAttribute("Table", Integer.toString(id));
					
				}*/
				
				//TODO Action Management
				final Element action = document.createElement("Action");
				table.appendChild(action);
				boolean clear = false;
				boolean goto_table = false;
				boolean apply_action = false;
				ArrayList<String> port_list = new ArrayList<String>(); // list des output ports
				ArrayList<TagType> push_tag_list = new ArrayList<TagType>(); // list des push_tags
				ArrayList<TagType> pop_tag_list = new ArrayList<TagType>(); // list des pop_tags
				ArrayList<FieldType> field_list = new ArrayList<FieldType>(); // list des fields
				ArrayList<MetaType> meta_list = new ArrayList<MetaType>(); // list des fields
				for (LogicalTable lt : mapping.get(pt)){                                    
					// Check if CLEAR_ACTIONS
					if (lt.getAction().isClear_all()){
						if (clear == false){
							final Element clearactions = document.createElement("CLEAR_ACTIONS");
							action.appendChild(clearactions);
							clear = true;
						}
					}
					// Check if APPLY_ACTIONS
					if (lt.getAction().getApply_Action() != null){
						final Element applyactions = document.createElement("APPLY_ACTIONS");
						action.appendChild(applyactions);

						// Output
						Set<String> ports = lt.getAction().getApply_Action().getSet_ports();
						if (ports != null){
							for (String out : ports){
								if (!port_list.contains(out)){
									final Element output = document.createElement("OUTPUT");
									applyactions.appendChild(output);
									output.setAttribute("Port", out);
									port_list.add(out);
								}
							}
						}
						// Set field
						Set<FieldType> fields = lt.getAction().getApply_Action().getSet_set_field();
						if (fields != null){
							for (FieldType f : fields){
								if (!field_list.contains(f)){
									final Element setfield = document.createElement("SET_FIELD");
									applyactions.appendChild(setfield);
									setfield.setAttribute("Field", f.name());
									field_list.add(f);
								}
							}
						}
						// Set meta
						Set<MetaType> metas = lt.getAction().getApply_Action().getSet_set_meta();
						if (metas != null){
							for (MetaType m : metas){
								if (!meta_list.contains(m)){
									final Element setfmeta = document.createElement("SET_META");
									applyactions.appendChild(setfmeta);
									setfmeta.setAttribute("Meta", m.name());
									meta_list.add(m);
								}
							}
						}
						// Push tag
						Set<TagType> tags = lt.getAction().getApply_Action().getSet_push_tag();
						if (tags != null){
							for (TagType t : tags){
								if (!push_tag_list.contains(t)){
									final Element pushtag = document.createElement("PUSH_TAG");
									applyactions.appendChild(pushtag);
									pushtag.setAttribute("Tag", t.name());
									push_tag_list.add(t);
								}
							}
						}
						// Pop tag
						Set<TagType> ptags = lt.getAction().getApply_Action().getSet_pop_tag();
						if (ptags != null){
							for (TagType t : ptags){
								if (!pop_tag_list.contains(t)){
									final Element poptag = document.createElement("POP_TAG");
									applyactions.appendChild(poptag);
									poptag.setAttribute("Tag", t.name());
									pop_tag_list.add(t);
								}
							}
						}
						
					}
					// GOTO Setting
					/*if (mapping_id < mapping.size()){
						if (goto_table == false ){
							final Element gototable = document.createElement("GOTO");
							action.appendChild(gototable);
							goto_table = true;
							gototable.setAttribute("Table", Integer.toString(mapping_id));
						}
						
					}*/
                                        // Check if goto table
					if (!lt.getAction().getNext_table().isEmpty()){
						//if (goto_table == false ){					
							
						        
                                                        for(int i=0;i<lt.getAction().getNext_table().size();i++){
                                                        final Element gototable = document.createElement("GOTO");
							action.appendChild(gototable);
                                                        gototable.setAttribute("Table",reversemapping.get(lt.getAction().getNext_table().get(i))+"");
                                                        }
						//}
					}
                                        
                                        
                                        
                                        
				}
				
			
			}
			
		}
		
		// Affichage du resultat
		final TransformerFactory transformerFactory = TransformerFactory.newInstance();
		final Transformer transformer = transformerFactory.newTransformer();
		
		final DOMSource source = new DOMSource(document);
		//Code à utiliser pour afficher dans un fichier
		final StreamResult sortie = new StreamResult(new File(mappingPath));

		//Code à utiliser pour afficher dans la console
		//final StreamResult sortie = new StreamResult(System.out);
		
		//prologue
	    transformer.setOutputProperty(OutputKeys.VERSION, "1.0");
	    transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
	    transformer.setOutputProperty(OutputKeys.STANDALONE, "yes");
	    
	    //formatage
	    transformer.setOutputProperty(OutputKeys.INDENT, "yes");
	    transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");
			
	    //sortie
	    transformer.transform(source, sortie);	
	    
		
		return ret;
		
	}
	
}
