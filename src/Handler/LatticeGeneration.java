package Handler;

import java.io.IOException;

public class LatticeGeneration {

	int id;

	public LatticeGeneration(int id) {
		super();
		this.id = id;
	}

	void GenerateLattice(String inputFilenameRCF, String LatticeXMLFileName)
			throws IOException, InterruptedException {

		int LatticeMinSupport = 0;
		java.lang.Runtime rt = java.lang.Runtime.getRuntime();
		String ofcompilerpath = "/opt/ltiral/";
		/** To review after **/
		String coron = " -names -order -alg:dtouch -method:snow -ext -xml -of:";// /coron-0.8/Myexample/tmp0.xml
		String generatexml = new StringBuilder().append(ofcompilerpath)
				.append("coron-0.8/core03_leco.sh ")
				.append(inputFilenameRCF + " ").append(LatticeMinSupport)
				.append(coron)
				.append(LatticeXMLFileName).toString();

		try {
			Process p = rt.exec(generatexml);
			p.waitFor();

		} catch (IOException e) {
			e.printStackTrace();
			System.out.println(" can't generate xml file");
		} catch (InterruptedException e) {
			e.printStackTrace();

			// String cmd = "./script/script_pour_coron.sh "+ ofcompilerpath +
			// inputFilenameRCF + " " + LatticeMinSupport + " lattice";
			// Process pc = rt.exec(cmd);
			// pc.waitFor();
		}
	}
}
