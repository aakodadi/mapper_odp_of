package struct;

import java.util.Map;

public class Header {

	int id;
	String EtherType;
	Map<Integer,Field> Fields;
	
	public Header(int id, String etherType, Map<Integer, Field> fields) {
		super();
		this.id = id;
		EtherType = etherType;
		Fields = fields;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getEtherType() {
		return EtherType;
	}

	public void setEtherType(String etherType) {
		EtherType = etherType;
	}

	public Map<Integer, Field> getFields() {
		return Fields;
	}

	public void setFields(Map<Integer, Field> fields) {
		Fields = fields;
	}

	@Override
	public String toString() {
		String result = "Header [id=" + id + ", EtherType=" + EtherType  + "]\n";
		result+="list of fields is :\n";
		for(Map.Entry<Integer,Field> e : Fields.entrySet()){
		result = result+e.toString()+"\n";	
	}
		return result;		
}

}