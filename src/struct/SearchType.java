package struct;

public enum SearchType {

    EXACT(10),
    LPM(20),
    TERNARY(30);
    int weight;

    SearchType(int weight) {
        this.weight = weight;
    }

    public int getWeight() {
        return weight;
    }
    
    
}
