package struct;

public class Field {

	int id;
	int size;
	FieldType type;
	
	
	public Field(int id, int size, FieldType type){
		this.id = id;
		this.size = size;
		this.type = type;			
	}


	@Override
	public String toString() {
		return "Field [id=" + id + ", size=" + size 
				+ ", type=" + type + "]";
	}


	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public int getSize() {
		return size;
	}


	public void setSize(int size) {
		this.size = size;
	}


	public FieldType getType() {
		return type;
	}


	public void setType(FieldType type) {
		this.type = type;
	}
	
}
