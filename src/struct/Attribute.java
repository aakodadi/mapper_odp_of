package struct;

import java.util.Map;

public class Attribute {

	int id;
	Map <Integer, LogicalTable> logical_tables; 
		
	public Attribute(int id, Map <Integer, LogicalTable> logical_tables) {
		// TODO Auto-generated constructor stub
		this.id = id;
		this.logical_tables= logical_tables;
	}

	@Override
	public String toString() {
		return "Attribute [id=" + id + ", logical_tables=" + logical_tables.toString()
				+ "]";
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Map<Integer, LogicalTable> getLogical_tables() {
		return logical_tables;
	}

	public void setLogical_tables(Map<Integer, LogicalTable> logical_tables) {
		this.logical_tables = logical_tables;
	}
	

}
