package struct;

import java.util.List;

public class Action {
	List<Integer>  next_table;
	boolean clear_all;
	ApplyActions apply_Action;
	public Action(List<Integer> next_table, boolean clear_all,ApplyActions apply_Action) {
		super();
		this.next_table = next_table;
		this.clear_all = clear_all;
		this.apply_Action = apply_Action;
	}
	public ApplyActions getApply_Action() {
		return apply_Action;
	}
	public void setApply_Action(ApplyActions apply_Action) {
		this.apply_Action = apply_Action;
	}
	public Action(List<Integer> next_table, boolean clear_all) {
		super();
		this.next_table = next_table;
		this.clear_all = clear_all;
		this.apply_Action = null;
	}
	public List<Integer> getNext_table() {
		return next_table;
	}
	public void setNext_table(List<Integer> next_table) {
		this.next_table = next_table;
	}
	public boolean isClear_all() {
		return clear_all;
	}
	public void setClear_all(boolean clear_all) {
		this.clear_all = clear_all;
	}
	
	
	
	
	
	@Override
	public String toString() {
		String result = "Action is : ";
		for(int i=0;i<this.next_table.size();i++){
			result+=" "+next_table.get(i).toString();
		}
		result+=this.apply_Action.toString();
		return result;
	}
	
	}

	

