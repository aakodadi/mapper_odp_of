package struct;

import java.util.Map;

public class LogicalTable {

	int id;
	SearchType required_search;
	int required_size; //(in bits)
	int required_capacity;//(number of entries)
	Map<Integer, Header> Matchfields;
	Action action;

	
	/**
	 * @return the required_search
	 */
	public SearchType getRequired_search() {
		return required_search;
	}



	/**
	 * @param required_search the required_search to set
	 */
	public void setRequired_search(SearchType required_search) {
		this.required_search = required_search;
	}



	/**
	 * @return the required_capacity
	 */
	public int getRequired_capacity() {
		return required_capacity;
	}



	/**
	 * @param required_capacity the required_capacity to set
	 */
	public void setRequired_capacity(int required_capacity) {
		this.required_capacity = required_capacity;
	}



	/**
	 * @return the matchfields
	 */
	public Map<Integer, Header> getMatchfields() {
		return Matchfields;
	}



	/**
	 * @param matchfields the matchfields to set
	 */
	public void setMatchfields(Map<Integer, Header> matchfields) {
		Matchfields = matchfields;
	}



	public int getRequired_size() {
		return required_size;
	}



	public void setRequired_size(int required_size) {
		this.required_size = required_size;
	}



	public LogicalTable(int id, SearchType required_search, int required_size,
			int required_capacity, Map<Integer, Header> matchfields) {
		super();
		this.id = id;
		this.required_search = required_search;
		this.required_size = required_size;
		this.required_capacity = required_capacity;
		Matchfields = matchfields;
		this.action=null;
	}



	public LogicalTable(int id, SearchType required_search, int required_size,
			int required_capacity, Map<Integer, Header> matchfields,
			Action action) {
		super();
		this.id = id;
		this.required_search = required_search;
		this.required_size = required_size;
		this.required_capacity = required_capacity;
		Matchfields = matchfields;
		this.action = action;
	}



	@Override
	public String toString() {
		String result= "LogicalTable [id=" + id + ", required_search="
				+ required_search + ", required_size=" + required_size
				+ ", required_capacity=" + required_capacity+ "]\n";
		result+="The list of headers is : \n";
		for(Map.Entry<Integer, Header>  e : Matchfields.entrySet()){
			result = result + e.toString()+"\n";
		}
		result+=action.toString();
		return result;
	}



	public int getId() {
		return id;
	}



	public void setId(int id) {
		this.id = id;
	}



	public Action getAction() {
		return action;
	}



	public void setAction(Action action) {
		this.action = action;
	}
	
	
}
