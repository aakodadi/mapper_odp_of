package struct;

import java.util.Map;

public class Object {

	int id;
	Map <Integer, PhysicalTable> list_physical_tables;
	public Object(int id,Map <Integer, PhysicalTable> list_physical_tables){
		this.id = id;
		this.list_physical_tables = list_physical_tables;
				
	}
	@Override
	public String toString() {
		return "Object [id=" + id + ", list_physical_tables="
				+ list_physical_tables.toString() + "]";
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public Map<Integer, PhysicalTable> getList_physical_tables() {
		return list_physical_tables;
	}
	public void setList_physical_tables(
			Map<Integer, PhysicalTable> list_physical_tables) {
		this.list_physical_tables = list_physical_tables;
	}
	
}
