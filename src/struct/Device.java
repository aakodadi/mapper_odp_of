package struct;

import java.util.Map;

public class Device {

	int id;
	public String label;
	String Description;
	//DeviceType System; //Enumeration
	int capacity;
	int clock;
	int interface_f;
	
	Map<Integer, PhysicalTable> tables;

	public Map<Integer, PhysicalTable> getTables() {
		return tables;
	}

	public void setTables(Map<Integer, PhysicalTable> tables) {
		this.tables = tables;
	}

	public Device(int id, String label, String description,
			int capacity,
			int clock,
			int interface_f, Map<Integer, PhysicalTable> tables) {
		super();
		this.id = id;
		this.label = label;
		this.Description = description;
		this.capacity = capacity;
		this.clock = clock;
		this.interface_f = interface_f;
		this.tables = tables;
	}

	@Override
	public String toString() {
		String result = "Device [id=" + id + ", label=" + label ;

		for (Map.Entry<Integer,PhysicalTable> e : tables.entrySet()){
			result = result +"\n"+ e.toString();
		}
		return result;
	}

}
