package struct;

import java.util.Map;

public class Concept {

		int id;
		Map<Integer, LogicalTable> list_logical_tables;
		Map<Integer, PhysicalTable> list_physical_tables;
		double latency_evaluation; // the smallest, the best
		double size_evaluation; // the (smallest & positive), the best
		double capacity_evaluation; // the greatest, the best
		public Concept(int id, Map<Integer, LogicalTable> list_logical_tables,
				Map<Integer, PhysicalTable> list_physical_tables,
				double latency_evaluation, double size_evaluation,
				double capacity_evaluation) {
			super();
			this.id = id;
			this.list_logical_tables = list_logical_tables;
			this.list_physical_tables = list_physical_tables;
			this.latency_evaluation = latency_evaluation;
			this.size_evaluation = size_evaluation;
			this.capacity_evaluation = capacity_evaluation;
		}
		public Concept(){
			
		}
		@Override
		public String toString() {
			String result= "Concept [id=" + id +"]";
			result+="List of logical tables is : \n";
			for(Map.Entry<Integer, LogicalTable> e : list_logical_tables.entrySet()){
				result += e.toString()+"\n";
			}
			result+="List of physical tables is : \n";
			for(Map.Entry<Integer, PhysicalTable> e : list_physical_tables.entrySet()){
				result += e.toString()+"\n";
			}
			
			return result;
		}
		public int getId() {
			return id;
		}
		public void setId(int id) {
			this.id = id;
		}
		public Map<Integer, LogicalTable> getList_logical_tables() {
			return list_logical_tables;
		}
		public void setList_logical_tables(
				Map<Integer, LogicalTable> list_logical_tables) {
			this.list_logical_tables = list_logical_tables;
		}
		public Map<Integer, PhysicalTable> getList_physical_tables() {
			return list_physical_tables;
		}
		public void setList_physical_tables(
				Map<Integer, PhysicalTable> list_physical_tables) {
			this.list_physical_tables = list_physical_tables;
		}
		public double getLatency_evaluation() {
			return latency_evaluation;
		}
		public void setLatency_evaluation(double latency_evaluation) {
			this.latency_evaluation = latency_evaluation;
		}
		public double getSize_evaluation() {
			return size_evaluation;
		}
		public void setSize_evaluation(double size_evaluation) {
			this.size_evaluation = size_evaluation;
		}
		public double getCapacity_evaluation() {
			return capacity_evaluation;
		}
		public void setCapacity_evaluation(double capacity_evaluation) {
			this.capacity_evaluation = capacity_evaluation;
		}		
		
}
