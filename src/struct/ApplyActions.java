package struct;

import java.util.Iterator;
import java.util.Map;
import java.util.Set;

public class ApplyActions {

    Set<String> set_ports;
    Set<FieldType> set_set_field;
    Set<MetaType> set_set_meta;
    Set<TagType> set_pop_tag;
    Set<TagType> set_push_tag;

    public ApplyActions(Set<String> set_ports, Set<FieldType> set_field,
            Set<TagType> set_pop_tag, Set<TagType> set_push_tag, Set<MetaType> set_set_meta) {
        super();
        this.set_ports = set_ports;
        this.set_set_field = set_field;
        this.set_pop_tag = set_pop_tag;
        this.set_push_tag = set_push_tag;
        this.set_set_meta = set_set_meta;
    }

    public Set<String> getSet_ports() {
        return set_ports;
    }

    public void setSet_ports(Set<String> set_ports) {
        this.set_ports = set_ports;
    }

    public Set<FieldType> getSet_set_field() {
        return set_set_field;
    }

    public void setSet_set_field(Set<FieldType> set_set_field) {
        this.set_set_field = set_set_field;
    }

    public Set<MetaType> getSet_set_meta() {
        return set_set_meta;
    }

    public void setSet_set_meta(Set<MetaType> set_set_meta) {
        this.set_set_meta = set_set_meta;
    }

    public Set<TagType> getSet_pop_tag() {
        return set_pop_tag;
    }

    public void setSet_pop_tag(Set<TagType> set_pop_tag) {
        this.set_pop_tag = set_pop_tag;
    }

    public Set<TagType> getSet_push_tag() {
        return set_push_tag;
    }

    public void setSet_push_tag(Set<TagType> set_push_tag) {
        this.set_push_tag = set_push_tag;
    }

    @Override
    public String toString() {
        return "Apply_Action [set_ports=" + set_ports + ", set_set_field="
                + set_set_field + ", set_pop_tag=" + set_pop_tag
                + ", set_push_tag=" + set_push_tag + "]";

    }

}
