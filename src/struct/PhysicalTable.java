package struct;

public class PhysicalTable {
	
	int id;
	SearchType search;
	public TableType type;
	public Memory memory;
	private int size; //(in bits)
	private int number_entry;//(number of entries)
	private int capacity;
	private int speed; //(nanosecond)

	public PhysicalTable(int id, SearchType search, TableType type, int size,
			int number_entry,int capacity, int speed, Memory memory) {
		super();
		this.id = id;
		this.search = search;
		this.type = type;
		this.size = size;
		this.number_entry = number_entry;
		this.capacity = capacity;
		this.speed = speed;
		this.memory = memory;
	}

	/**
	 * @return the search
	 */
	public SearchType getSearch() {
		return search;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @param search the search to set
	 */
	public void setSearch(SearchType search) {
		this.search = search;
	}

	/**
	 * @return the size
	 */
	public int getSize() {
		return size;
	}

	/**
	 * @param size the size to set
	 */
	public void setSize(int size) {
		this.size = size;
	}

	/**
	 * @return the capacity
	 */
	public int getNbr_entries() {
		return number_entry;
	}

	/**
	 * @param capacity the capacity to set
	 */
	public void setNbr_entries(int number_entry) {
		this.number_entry = number_entry;
	}

	@Override
	public String toString() {
		return "PhysicalTable [id=" + id + ", search=" + search + ", size="
				+ size + ", capacity=" + number_entry + ", latency=" + speed
				+ "]";
	}
	
}
