package struct;

/**
 *
 * @author lined
 */
public enum FieldType {
	
	INGRESS_PORT, // Ingress Port
	DMAC, 		// Destination MAC
	SMAC, 		// Source MAC
	ETHER_TYPE, // Ethertype
	VLAN_ID, 	// Vlan id
	IP_PROTOCOL,  //
	SIP, 		// Source IP
	DIP, 		// Destination IP
	L4_SP, 		// Source Port
	L4_DP,
        TTL,
        Checksum;
	public int getLenght(){
		if(this.equals(INGRESS_PORT)){
			return 32;
		}
		if(this.equals(DMAC) || this.equals(SMAC)){
			return 48;
		}
		if(this.equals(SIP) || this.equals(DIP)){
			return 32;
		}
		if(this.equals(IP_PROTOCOL)){
			return 8;
		}
		if(this.equals(ETHER_TYPE) || this.equals(VLAN_ID) || this.equals(L4_DP) || this.equals(L4_SP) || this.equals(TTL) || this.equals(Checksum)){
			return 16;
		}
		return 0;
	}
	public SearchType getSearchType(){
		if(this.equals(SIP) || this.equals(DIP)){
			return SearchType.LPM;
		}
		else{
			return SearchType.EXACT;
		}		
	}
}
