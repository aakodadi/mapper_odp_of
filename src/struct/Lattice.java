package struct;

import java.util.Map;

public class Lattice {

	int id;
	Map<Integer, Concept> list_concept;
	public Lattice(int id,Map<Integer, Concept> list_concept ){
		this.id = id;
		this.list_concept = list_concept;
	}
	@Override
	public String toString() {
		String result = "Lattice [id=" + id + "] \n";
		for(Map.Entry<Integer, Concept> e : list_concept.entrySet()){
			result += e.toString() +"\n";
		}
		
		return result;
	}
	public Map<Integer, Concept> getList_concept() {
		return list_concept;
	}
	public void setList_concept(Map<Integer, Concept> list_concept) {
		this.list_concept = list_concept;
	}
	
}