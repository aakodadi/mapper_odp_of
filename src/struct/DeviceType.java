package struct;

public enum DeviceType {
	CONFIGURABLE,
	PROGRAMMABLE;
}
