package struct;

import java.util.Map;
import java.util.Map.Entry;

public class Application {

	int id;
	public String name;
	int Throughput_min;
	int Throughput_max;
	int Delay_min;
	int Delay_max;
	int tables_number;
	private Map<Integer, LogicalTable> Pipeline;
	
	public Application(int id, String name, int throughput_min,
			int throughput_max, int delay_min, int delay_max,
			Map<Integer, LogicalTable> pipeline) {
		super();
		this.id = id;
		this.name = name;
		Throughput_min = throughput_min;
		Throughput_max = throughput_max;
		Delay_min = delay_min;
		Delay_max = delay_max;
		setPipeline(pipeline);
	}
	@Override
	public String toString() {
		String result = "Application [id=" + id + ", name=" + name + ", Throughput_min="
				+ Throughput_min + ", Throughput_max=" + Throughput_max
				+ ", Delay_min=" + Delay_min + ", Delay_max=" + Delay_max+ "]";
		for (Entry<Integer, LogicalTable> e : getPipeline().entrySet()){
			result = result +"\n"+ e.toString();
		}
		
		return result;
	}
	public Map<Integer, LogicalTable> getPipeline() {
		return Pipeline;
	}
	public void setPipeline(Map<Integer, LogicalTable> pipeline) {
		Pipeline = pipeline;
	}
	
}
